package com.formation.demospring.controller;
import java.util.UUID;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LiveController {
	
	private final String uuid = UUID.randomUUID().toString();
	
	@GetMapping("/live")
	public String getId() {
		return "server "+ uuid;
	}
}
