# Utilisation d'une image OpenJDK 17 comme base
FROM openjdk:17

# Créer un répertoire pour l'application
RUN mkdir -p /app 

ARG JAR_FILE=target/*.jar
# Copie du fichier JAR de l'application Spring Boot dans le conteneur

COPY ${JAR_FILE} /app/app.jar


# Définition du répertoire de travail dans le conteneur
WORKDIR /app

# Commande pour démarrer l'application Spring Boot lorsque le conteneur démarre
CMD ["java", "-jar", "app.jar"]